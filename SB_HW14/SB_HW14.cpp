﻿// SB_HW14.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <math.h>


class DataExample {
private:

    int a;
    int b;
    int c;
public:

    int GetA() {
        return a;
    }

    int GetB() {
        return b;
    }

    int GetC() {
        return c;
    }

    void SetA(int newA) {
        a = newA;
}

    void SetB(int newB) {
        b = newB;
    }

    void SetC(int newC) {
        c = newC;
    }

};

class Vector {

private:
         
    double x; 
    double y; 
    double z;// vector coordinates
    double mod;

public:

  
    Vector() {}  // : x(0), y(0), z(0),mod()


    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) { }

      
    void ShowLenght() {

        mod = sqrt(x * x + y * y + z * z);   // method
        std::cout << "Vector Coordinates (xyz) is: " << x << ' ' << y << ' ' << z << '\n';
        std::cout  << "Vector Lenght is:" << ' ' << mod;

}

};

int main()
{
    DataExample  datatemp;

    int _x;
    int _y;
    int _z;

    std::cout << "SKILLBOX HW14!\n";
    std::cout << "Enter the coordinates (xyz):" << '\n';
    std::cout << "Enter X:" << '\n';
    std::cin >> _x;
    std::cout << "Enter Y:" << '\n';
    std::cin >> _y;
    std::cout << "Enter Z:" << '\n';
    std::cin >> _z;

    Vector  VectorLength(_x, _y, _z);
    VectorLength.ShowLenght();
    

    datatemp.SetA(7);
    datatemp.SetB(7);
    datatemp.SetC(7);
    
    std::cout << "\n";
   
    std::cout << datatemp.GetA() << ' ' << datatemp.GetB() << ' ' << datatemp.GetC();

       
    

}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
